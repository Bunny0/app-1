package com.sitthichai.app1

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.sitthichai.app1.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private  lateinit var binding: ActivityMainBinding

    companion object{
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val name : TextView = findViewById(R.id.textView_name)
        val id : TextView = findViewById(R.id.textView_id)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btn.setOnClickListener{
            Log.d(TAG,"name :"+name.text)
            Log.d(TAG,"id :"+id.text)
            val message = name.text
            val intent = Intent(this,HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE,message)
            }
            startActivity(intent)
        }
    }
}